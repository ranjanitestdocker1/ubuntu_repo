# Use an official Python runtime as a parent image
FROM ubuntu:14.04

#Runs the following ubuntu commands
RUN apt-get update
RUN apt-get upgrade -y

#Create working directory.From now on, all commands will run inside it.
#RUN mkdir -p /myapp
#WORKDIR /myapp

#To install python
RUN apt-get install -y python 
RUN apt-get install -y python-pip python-dev build-essential

#to fetch python file from ubuntu
#RUN "PYTHONPATH=/usr/lib/python3" >> ~/.bashrc
#CMD source ~/.bashrc && <other commands>

# Run app.py when the container launches
ENTRYPOINT ["python"]
CMD ["/home/ranjani/mycode/testdocker1.py"]

